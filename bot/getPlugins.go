package main

import (
	"fmt"
	"strings"
	"tbot"

	"gopkg.in/devalecs/tgo.v1"
)

type GetPluginsPlugin struct{}

func (p *GetPluginsPlugin) Reply(b tbot.B, update *tgo.Update) {

	b.SendMessage(tgo.SendMessageParams{
		ChatID: update.Message.Chat.ID,
		Text:   fmt.Sprintf("%s", b.GetPlugins()),
	})
}

func (p *GetPluginsPlugin) Match(b tbot.B, update *tgo.Update) bool {
	// fmt.Println(update.Message)
	m := strings.Split(update.Message.Text, " ")

	fmt.Println(m)
	return b.MessageTextMatch(update, "getPlugins")
}

func (p *GetPluginsPlugin) Settings() tbot.PluginSettings {
	return tbot.PluginSettings{
		Help: "*/getPlugins*: получить все плагины",
	}
}

func (p *GetPluginsPlugin) GetTitle() (title string) {
	return "GetPluginsPlugin"
}
