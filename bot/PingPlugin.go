package main

import (
	"tbot"

	"gopkg.in/devalecs/tgo.v1"
)

type PingPlugin struct{}

func (p *PingPlugin) Reply(b tbot.B, update *tgo.Update) {
	b.SendMessage(tgo.SendMessageParams{
		ChatID: update.Message.Chat.ID,
		Text:   "pong",
	})
}

func (p *PingPlugin) Match(b tbot.B, update *tgo.Update) bool {
	return b.MessageTextMatch(update, "ping")
}

func (p *PingPlugin) Settings() tbot.PluginSettings {
	return tbot.PluginSettings{
		Help: "*/ping*:  ping pong plugin",
	}
}
func (p *PingPlugin) GetTitle() (title string) {
	return "PingPlugin"
}
