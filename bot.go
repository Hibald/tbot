package tbot

import (
	"log"

	"gopkg.in/devalecs/tgo.v1"
)

type Bot struct {
	*tgo.Client
	*Tools
	config  Config
	plugins []Plugin
}

type Config struct {
	Token         string
	AdminUsername string
}

func NewBot(config Config) *Bot {
	return &Bot{
		config: config,
		Client: tgo.NewClient(config.Token),
	}
}

func (b *Bot) GetPlugins() []string {
	plugs := []string{}
	for _, j := range b.plugins {
		plugs = append(plugs, j.GetTitle())
	}

	return plugs
}

func (b *Bot) UserIsAdmin(user tgo.User) bool {
	return b.config.AdminUsername == user.Username
}

func (b *Bot) RegisterPlugins(plugins ...Plugin) {
	for _, plugin := range plugins {
		b.registerPlugin(plugin)
	}
}
func (b *Bot) UnRegisterPlugins(plugins ...Plugin) {
	for _, plugin := range plugins {
		b.unregisterPlugin(plugin)
	}
}

func (b *Bot) Start() error {
	user, err := b.GetMe()

	Errlog(err)

	log.Printf("Запускается %s\n", user.Username)

	updatesChan := b.GetUpdatesChan(tgo.GetUpdatesParams{
		Timeout: 60,
	})

	for update := range updatesChan {
		go func() {
			b.runPlugins(update)
		}()
	}
	return nil
}

func (b *Bot) registerPlugin(plugin Plugin) {
	b.plugins = append(b.plugins, plugin)

}

func (b *Bot) unregisterPlugin(plugin Plugin) {
	b.plugins = remove(b.plugins, plugin)

}

func Errlog(err error) {
	if err != nil {
		log.Println(err)
	}
}

func remove(slice []Plugin, s Plugin) (sl []Plugin) {
	sl = []Plugin{}
	for i, j := range slice {
		if j == s {
			sl = append(slice[:i], slice[i+1:]...)
		}
	}
	return sl
}

func (b *Bot) runPlugins(update *tgo.Update) {
	for _, p := range b.plugins {
		if p.Match(b, update) {
			p.Reply(b, update)
		}
	}
}
