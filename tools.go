package tbot

import (
	"regexp"

	"gopkg.in/devalecs/tgo.v1"
)

type Tools struct{}

func (t *Tools) MessageTextMatch(update *tgo.Update, pattern string) bool {
	if update.Message == nil {
		return false
	}

	match, _ := regexp.MatchString(pattern, update.Message.Text)

	return match
}

func (t *Tools) InlineQueryMatch(update *tgo.Update, pattern string) bool {
	if update.InlineQuery == nil {
		return false
	}

	match, _ := regexp.MatchString(pattern, update.InlineQuery.Query)

	return match
}

func (t *Tools) MessageTextMatchParams(update *tgo.Update, pattern string) (bool, []interface{}) {
	var submatch []interface{}

	if update.Message == nil {
		return false, submatch
	}

	r := regexp.MustCompile(pattern)
	if !r.MatchString(update.Message.Text) {
		return false, submatch
	}

	match := r.FindStringSubmatch(update.Message.Text)

	return true, t.stringToInterfaceSlice(match[1:])
}

func (t *Tools) InlineQueryMatchParams(update *tgo.Update, pattern string) (bool, []interface{}) {
	var submatch []interface{}

	if update.InlineQuery == nil {
		return false, submatch
	}

	r := regexp.MustCompile(pattern)
	if !r.MatchString(update.InlineQuery.Query) {
		return false, submatch
	}

	match := r.FindStringSubmatch(update.InlineQuery.Query)

	return true, t.stringToInterfaceSlice(match[1:])
}

func (t *Tools) stringToInterfaceSlice(stringSlices []string) []interface{} {
	var interfaceSlices []interface{}

	for _, stringSlice := range stringSlices {
		interfaceSlices = append(interfaceSlices, interface{}(stringSlice))
	}

	return interfaceSlices
}
